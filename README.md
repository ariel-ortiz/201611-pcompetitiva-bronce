# Código de la división bronce

Este proyecto contiene los archivos fuente de las sesiones de entrenamiento de la **división de bronce** del grupo de programación competitiva del Tecnológico de Monterrey, Campus Estado de México, correspondientes al semestre **enero-mayo del 2016**.

Los archivos se pueden consultar directamente en este mismo sitio, o se puede usar git para obtener una copia local de éstos. En este último caso se deben seguir los siguiente pasos:

 1. Si es necesario, [instalar un cliente git](http://git-scm.com/downloads) en tu computadora. La plataforma [Cloud9](http://c9.io/) ya cuenta con git instalado.

 2. Clonar este repositorio. Desde la terminal teclear:
    
        git clone https://bitbucket.org/ariel-ortiz/201611-pcompetitiva-bronce.git problemas
    
 3. Cambiarse al directorio `problemas`:
    
        cd problemas
    
    En dicho directorio encontrás todos los archivos fuente del proyecto.
    
 4. Cada vez que el profesor realice modificaciones a archivos existentes o agregue archivos nuevos, será necesario hacer un *pull* al repositorio. En la terminal y desde el directorio `problemas` teclear: 
    
        git pull
