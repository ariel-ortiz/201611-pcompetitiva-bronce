import java.util.Scanner;

// La clase principal siempre se debe llamar Main.
public class Main {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in)) {
            while (in.hasNextInt()) {
                int x = in.nextInt();
                if (x == 42) {
                    break;
                }
                System.out.println(x);
            }
        }
    }
}