# 1156 - Life, the Universe, and Everything

http://coj.uci.cu/24h/problem.xhtml?pid=1156

## Python

    $ python 1156.py < 1.in
    1
    2
    88
    $ python 1156.py < 2.in
    93
    67
    52
    53
    78
    97
    4
    98
    89
    $ python 1156.py < 3.in
    3
    64
    33
    40
    5
    94
    30
    43
    31
    40
    88
    74
    13
    
## C

    $ gcc 1156.c -o 1156
    $ ./1156 < 1.in    
    1
    2
    88
    $ ./1156 < 2.in
    93
    67
    52
    53
    78
    97
    4
    98
    89
    $ ./1156 < 3.in
    3
    64
    33
    40
    5
    94
    30
    43
    31
    40
    88
    74
    13
    
## Java    

    $ javac Main.java 
    $ java Main < 1.in
    1
    2
    88
    $ java Main < 2.in
    93
    67
    52
    53
    78
    97
    4
    98
    89
    $ java Main < 3.in
    3
    64
    33
    40
    5
    94
    30
    43
    31
    40
    88
    74
    13