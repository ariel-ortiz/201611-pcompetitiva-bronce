#include <stdio.h>

int main(void) {
    int x;
    while (scanf("%d", &x) != EOF && x != 42) {
        printf("%d\n", x);
    }
    return 0;
}