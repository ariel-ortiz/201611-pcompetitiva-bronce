# coding: utf-8

from sys import stdin

# Leer todo el contenido completo de entrada estándar.
entrada = stdin.read()

# Convertir cada línea a un entero.
nums = [int(x) for x in entrada.split()]

for x in nums:
    if x == 42:
        break
    print x
