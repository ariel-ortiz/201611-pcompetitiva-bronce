# 1000 - A+B Problem

http://coj.uci.cu/24h/problem.xhtml?pid=1000

## Python

    $ python 1000.py < 1.in 
    3
    $ python 1000.py < 2.in
    20
    $ python 1000.py < 3.in
    0
    
## C

    $ gcc 1000.c -o 1000
    $ ./1000 < 1.in
    3
    $ ./1000 < 2.in
    20
    $ ./1000 < 3.in
    0
    
## Java

    $ javac Main.java
    $ java Main < 1.in
    3
    $ java Main < 2.in
    20
    $ java Main < 3.in
    0
