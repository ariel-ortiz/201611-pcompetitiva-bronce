import java.util.Scanner;

public class acronyms {
    
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        while (s.hasNextLine()) {
            String linea1 = s.nextLine();
            String linea2 = s.nextLine();
            String[] palabras1 = linea1.split(" ");
            String[] palabras2 = linea2.split(" ");
            if (palabras1.length == palabras2.length) {
                boolean discrepancia = false;
                for (int i = 0; i < palabras1.length; i++) {
                    if (palabras1[i].charAt(0) != palabras2[i].charAt(0)) {
                        discrepancia = true;
                        break;
                    }
                }
                if (discrepancia) {
                    System.out.println("no");
                } else {
                    System.out.println("yes");
                }
            } else {
                System.out.println("no");
            }
        }
    }
}