# Lista de problemas

## 30-Mar-2016
* [2634 - Birthday Statistics](http://coj.uci.cu/24h/problem.xhtml?pid=2634)
* [1164 - Heptadecimal Numbers](http://coj.uci.cu/24h/problem.xhtml?pid=1164)
* [1688 - Eazzzzzy](http://coj.uci.cu/24h/problem.xhtml?pid=1688)

## 16-Mar-2016
* [1189 - Skew Binary](http://coj.uci.cu/24h/problem.xhtml?pid=1189)
* [1136 - Prime Generator](http://coj.uci.cu/24h/problem.xhtml?pid=1136)
* [1328 - CAVerage](http://coj.uci.cu/24h/problem.xhtml?pid=1328)

## 09-Mar-2016
* [1655 - Xtreme Encription](http://coj.uci.cu/24h/problem.xhtml?pid=1655)
* [1355 - The Cross I](http://coj.uci.cu/24h/problem.xhtml?pid=1355)
* [1626 - Adding Reversed Numbers](http://coj.uci.cu/24h/problem.xhtml?pid=1626)

## 24-Feb-2016
* [1071 - Bishops](http://coj.uci.cu/24h/problem.xhtml?pid=1071)
* [2380 - BinaryCheckSum](http://coj.uci.cu/24h/problem.xhtml?pid=2380)
* [1215 - Binary Clock](http://coj.uci.cu/24h/problem.xhtml?pid=1215)
* [1143 - To and Fro](http://coj.uci.cu/24h/problem.xhtml?pid=1143)

## 17-Feb-2016
* [2115 - Hot Dogs](http://coj.uci.cu/24h/problem.xhtml?pid=2115)
* [1840 - Broken Strings](http://coj.uci.cu/24h/problem.xhtml?pid=1840)
* [1101 - Binaries Palindromes](http://coj.uci.cu/24h/problem.xhtml?pid=1101)
* [1237 - Mean Median Problem](http://coj.uci.cu/24h/problem.xhtml?pid=1237)

## 10-Feb-2016
* [1066 - Regular Polygons](http://coj.uci.cu/24h/problem.xhtml?pid=1066)
* [1573 - Just Another Easy Problem](http://coj.uci.cu/24h/problem.xhtml?pid=1573)
* [1808 - Hamming Distance](http://coj.uci.cu/24h/problem.xhtml?pid=1808)
* [1324 - Snow White](http://coj.uci.cu/24h/problem.xhtml?pid=1324)

## 03-Feb-2016

* [2091 - Counting Task](http://coj.uci.cu/24h/problem.xhtml?pid=2091)
* [1118 - The Drunk Jailer](http://coj.uci.cu/24h/problem.xhtml?pid=1118)
* [2148 - Quadratic Equations](http://coj.uci.cu/24h/problem.xhtml?pid=2148)
* [2152 - Sum of the Digits](http://coj.uci.cu/24h/problem.xhtml?pid=2152)

## 27-Ene-2016

* [1252 - The Seven Percent Solution](http://coj.uci.cu/24h/problem.xhtml?pid=1252)
* [1326 - Account Balance](http://coj.uci.cu/24h/problem.xhtml?pid=1326)
* [1842 - Distance of Manhattan](http://coj.uci.cu/24h/problem.xhtml?pid=1842)
* [2698 - A+B=C](http://coj.uci.cu/24h/problem.xhtml?pid=2698)

## Problemas introductorios

* [1000 - A+B Problem](http://coj.uci.cu/24h/problem.xhtml?pid=1000)
* [1156 - Life, the Universe, and Everything](http://coj.uci.cu/24h/problem.xhtml?pid=1156)