# coding: utf-8

# Al parecer no funciona si se intenta leer y procesar una 
# línea de texto a la vez. En su lugar, este programa lee 
# toda la entrada de un solo golpe y luego procesa un 
# caracter a la vez.

from sys import stdin, stdout

tabla = { ' ': '%20', '!': '%21', '$': '%24', '%': '%25', 
          '(': '%28', ')': '%29', '*': '%2a' }

for c in stdin.read():
    if c == '#':
        break
    stdout.write(tabla[c] if c in tabla else c)